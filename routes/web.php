<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get ("/naujienos", "NewsController@index");

//paprastas kelias
Route::get('/about-us', function() {
    return view('about');
});

//kad kreiptis butent i index funkcija @index
Route::get('/contacts', 'HomeController@index');

/*
//kelias su parametrais
Route::get('/naujienos/{id}', function($id) {
    echo $id;
    return view('about');
});

*/

Route::get('/skaiciuokle', 'HomeController@skaiciuokle' );

Route::post('/suma', 'HomeController@suma')->name('suma');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



Route::get('/testas', function() {
    return view('test');
});
