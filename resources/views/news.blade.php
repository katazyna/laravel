@extends("layouts.app")

@section('content')
<div class="container">
    <h1> Labas</h1>

    @foreach($news as $newsItem)
        <div>
        <h3>
        {{ $newsItem->title }}
        </h3>
        <p>
        {{ $newsItem->content }}
        </p>
        </div>
    @endforeach
</div>
@endsection
